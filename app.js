const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();

const port = 3000;


app.listen(port, ()=> {
  console.log('Server listening on port: ' + port);
});

app.use(express.static(path.join(__dirname, 'public')));
router.get('/', (req, res) => {
  res.sendFile('index')
});